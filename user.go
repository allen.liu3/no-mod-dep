package nomoddep

import (
	"log"

	validator "github.com/go-playground/validator/v10"
)

func NewUser() User {
	u := User{
		Name: "hello",
	}

	v := validator.New()
	err := v.Struct(u)
	if err != nil {
		log.Println(err)
	}

	return u
}

type User struct {
	Name string
}
